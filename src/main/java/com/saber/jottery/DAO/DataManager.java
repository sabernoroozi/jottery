package com.saber.jottery.DAO;

import com.saber.jottery.File.FileManager;
import com.saber.jottery.model.InputValues;
import com.saber.jottery.model.OutPutValues;
import com.saber.jottery.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataManager {

    private FileManager fileManager;
    private CsvRepository csvRepository;

    public DataManager() {
    }

    @Autowired
    public DataManager(FileManager fileManager, CsvRepository csvRepository) {
        this.fileManager = fileManager;
        this.csvRepository = csvRepository;
    }

    public void writeToDb(){
        for(String[] str:fileManager.readCsv()){
            Person person = new Person();
            person.setCode(str[0]);
            person.setIdNumber(str[1]);
            person.setFirstName(str[2]);
            person.setLastName(str[3]);
            person.setScore(Integer.parseInt(str[4]));
            person.setBirthDate(str[5]);
            person.setGender(str[6]);
            person.setPhoneNumber(str[7]);
            csvRepository.save(person);
        }
    }

    public void deleteAll(){
        csvRepository.deleteAll();
    }

    public Iterable<Person> findAll(){
        return csvRepository.findAll();
    }

    public List<OutPutValues> classify(List<InputValues> inputValues){
        List<OutPutValues> outPutValues = new ArrayList<>();
        for(InputValues in: inputValues){
            int min = Integer.parseInt(in.getFrom());
            int max = Integer.parseInt(in.getTo());
            Long out = csvRepository.queryCount(min, max);
            OutPutValues outPutValue = new OutPutValues(in.getName(), out);
            outPutValues.add(outPutValue);
        }
        return outPutValues;
    }
}
