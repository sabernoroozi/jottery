package com.saber.jottery.DAO;

import com.saber.jottery.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CsvRepository extends CrudRepository<Person, Long> {
    @Query(value = "select count (*) from person where score between ?1 and ?2", nativeQuery = true)
    public Long queryCount(int min, int max);
}
