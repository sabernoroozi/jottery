package com.saber.jottery.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PersonId implements Serializable {
    private Long id;
    private String code;
    private String idNumber;
    private String phoneNumber;

    public PersonId() {
    }

    public PersonId(Long id, String code, String idNumber, String phoneNumber) {
        this.id = id;
        this.code = code;
        this.idNumber = idNumber;
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonId personId = (PersonId) o;
        return Objects.equals(id, personId.id) &&
                Objects.equals(code, personId.code) &&
                Objects.equals(idNumber, personId.idNumber) &&
                Objects.equals(phoneNumber, personId.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, idNumber, phoneNumber);
    }
}
