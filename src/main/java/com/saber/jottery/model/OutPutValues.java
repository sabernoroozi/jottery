package com.saber.jottery.model;

import org.springframework.stereotype.Component;

public class OutPutValues {
    private String name;
    private Long count;

    public OutPutValues() {
    }

    public OutPutValues(String name, Long count) {
        this.name = name;
        this.count = count;
    }

    public OutPutValues(String name) {
        this.name = name;
    }

    public OutPutValues(Long count) {
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
