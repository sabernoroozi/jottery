package com.saber.jottery.view;

import com.saber.jottery.controller.Manager;
import com.saber.jottery.model.InputValues;
import com.saber.jottery.model.OutPutValues;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Route
public class View extends VerticalLayout {
    private Manager manager;
    private List<InputValues> inputValues = new ArrayList<>();
    @Autowired
    public View(Manager manager) {
        this.manager = manager;
        inputValues = new ArrayList<>();
    }

    @PostConstruct
    private void start(){
        Label label = new Label("Results");
        Grid<OutPutValues> grid = new Grid<>(OutPutValues.class);
        grid.setHeightByRows(true);
        Button button = new Button("process", event-> {
            for(InputValues in: inputValues){
                List<OutPutValues> outValues = manager.classify(inputValues);
//                grid.removeAllColumns();
                grid.setDataProvider(DataProvider.ofCollection(outValues));
            }
        });
        add(label, addHorizontal("A-Level"), addHorizontal("B-Level"), addHorizontal("C-Level"), button, grid);
    }

    private HorizontalLayout addHorizontal(String s){
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.setAlignItems(Alignment.CENTER);
        Label label = new Label(s);
        TextField textField = new TextField("From");
        TextField textField1 = new TextField("To");
        Button button = new Button("ADD", event-> {
            InputValues input = new InputValues(s, textField.getValue(), textField1.getValue());
            inputValues.add(input);
        });
        horizontalLayout.add(label, textField, textField1, button);
        return horizontalLayout;
    }

}
