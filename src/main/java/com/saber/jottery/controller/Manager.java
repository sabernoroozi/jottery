package com.saber.jottery.controller;

import com.saber.jottery.DAO.DataManager;
import com.saber.jottery.model.InputValues;
import com.saber.jottery.model.OutPutValues;
import com.saber.jottery.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public class Manager {
    private DataManager dataManager;

    @Value(value = "${data.wipe}")
    private String wipe_data;

    @Value(value = "${data.import}")
    private String data_import;

    public Manager() {
    }

    @Autowired
    public Manager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @PostConstruct
    private void starter(){
        if(wipe_data.equals("true")){
            deleteAll();
        }
        if(data_import.equals("true")){
            writeToDb();
        }
    }

    public void writeToDb(){
        dataManager.writeToDb();
    }

    public void deleteAll(){
        dataManager.deleteAll();
    }

    public Iterable<Person> findAll(){
        return dataManager.findAll();
    }

    public List<OutPutValues> classify(List<InputValues> inputValues){
        return dataManager.classify(inputValues);
    }
}
