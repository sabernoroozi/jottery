package com.saber.jottery.File;

import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:application.properties")
public class FileManager {
    @Value("${address}")
    private String address;

//    private Path path = Paths.get(address);


    public FileManager() {
    }

    public List<String[]> readCsv() {
        Reader reader = null;
        Path path = Paths.get(address);
        List<String[]> list = null;
        try {
            reader = Files.newBufferedReader(path);
            CSVReader csvReader = new CSVReader(reader);
            list = new ArrayList<>(1000);
            list = csvReader.readAll();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
