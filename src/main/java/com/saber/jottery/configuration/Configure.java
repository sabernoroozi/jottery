package com.saber.jottery.configuration;

import com.saber.jottery.File.FileManager;
import com.saber.jottery.model.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Configure {

    @Bean
    public Person getPerson(){
        return new Person();
    }
}
